import 'dart:io';

checklottery(String n) {
  var first = "436594";
  var two_suffix = "14";
  bool boolean = false;
  List<String> neighbors = ["436593", "436595"];
  List<String> three_prefix = ["266", "893"];
  List<String> three_suffix = ["447", "282"];
  List<String> second = ["285563", "084971", "396501", "502412", "049364"];
  List<String> third = [
    "575619",
    "422058",
    "666926",
    "662884",
    "242496",
    "166270",
    "996939",
    "043691",
    "896753",
    "853019"
  ];
  List<String> fourth = [
    "051550",
    "511577",
    "513197",
    "223796",
    "327644",
    "696914",
    "181516",
    "811661",
    "651710",
    "610343",
    "927245",
    "267790",
    "451697",
    "306091",
    "263978",
    "845036",
    "671874",
    "675634",
    "502070",
    "450860",
    "966337",
    "200726",
    "550340",
    "567966",
    "991993",
    "383689",
    "240354",
    "345541",
    "403115",
    "011052",
    "917404",
    "799229",
    "235563",
    "525227",
    "691190",
    "400877",
    "957886",
    "124121",
    "451966",
    "646531",
    "278267",
    "719971",
    "491669",
    "398596",
    "098790",
    "539598",
    "144105",
    "816411",
    "355409",
    "293437"
  ];

  List<String> fifth = [
    "571222",
    "787212",
    "166929",
    "800417",
    "694967",
    "849785",
    "438861",
    "715135",
    "505423",
    "564651",
    "695414",
    "312724",
    "865020",
    "758499",
    "192145",
    "234349",
    "345587",
    "328183",
    "581874",
    "006954",
    "697291",
    "124285",
    "292486",
    "075960",
    "091833",
    "743359",
    "229529",
    "399645",
    "219615",
    "668528",
    "296893",
    "801225",
    "581954",
    "622693",
    "286524",
    "193683",
    "806157",
    "361660",
    "110635",
    "974474",
    "528438",
    "239819",
    "477473",
    "847630",
    "358839",
    "096228",
    "041233",
    "482629",
    "445118",
    "714242",
    "059236",
    "878866",
    "990959",
    "321889",
    "944523",
    "236234",
    "545548",
    "687291",
    "155735",
    "904680",
    "961523",
    "613293",
    "033687",
    "748921",
    "571174",
    "047113",
    "378959",
    "668067",
    "156252",
    "506857",
    "144481",
    "282906",
    "301244",
    "623231",
    "028671",
    "029793",
    "665460",
    "208198",
    "169194",
    "956187",
    "253367",
    "116886",
    "127449",
    "761545",
    "737379",
    "896268",
    "798548",
    "690182",
    "426761",
    "259049",
    "206095",
    "007464",
    "379191",
    "088082",
    "871886",
    "198703",
    "004758",
    "063875",
    "923777",
    "319735"
  ];

  if (first == n) {
    return print("First Prize: $n Prize 6,000,000 Baht");
  } else if (n.substring(4, 6) == two_suffix) {
    var num1 = n.substring(4, 6);
    return print("Two Digit Suffix: $num1 Prize 2,000 Baht");
  } else {
    for (var i = 0; i < three_prefix.length; i++) {
      if (three_prefix.isEmpty == false) {
        if (n.substring(0, 3) == three_prefix[i]) {
          var num2 = n.substring(0, 3);
          return print("Three Digit Prefix: $num2 Prize 4,000 Baht");
          break;
        }
      }
    }

    for (var i = 0; i < three_suffix.length; i++) {
      if (three_suffix.isEmpty == false) {
        if (n.substring(3, 6) == three_suffix[i]) {
          var num3 = n.substring(3, 6);
          return print("	Three Digit Suffix: $num3 Prize 4,000 Baht");
          break;
        }
      }
    }

    for (var i = 0; i < neighbors.length; i++) {
      if (neighbors.isEmpty == false) {
        if (n == neighbors[i]) {
          return print("First Prize Neighbors: $n Prize 100,000 Baht");
          break;
        }
      }
    }

    for (var i = 0; i < second.length; i++) {
      if (second.isEmpty == false) {
        if (n == second[i]) {
          return print("Second Prize: $n Prize 200,000 Baht");
          break;
        }
      }
    }

    for (var i = 0; i < third.length; i++) {
      if (third.isEmpty == false) {
        if (n == third[i]) {
          return print("Third Prize: $n Prize 80,000 Baht");
          break;
        }
      }
    }

    for (var i = 0; i < fourth.length; i++) {
      if (fourth.isEmpty == false) {
        if (n == fourth[i]) {
          return print("Fourth Prize: $n Prize 40,000 Baht");
          break;
        }
      }
    }

    for (var i = 0; i < fifth.length; i++) {
      if (fifth.isEmpty == false) {
        if (n == fifth[i]) {
          return print("Fifth Prize: $n Prize 20,000 Baht");
          break;
        }
      }
    }

    return print("Unfortunately, you didn't get the reward.");
  }
}

void main() {
  print("Enter number 6 lottery: ");
  var n = (stdin.readLineSync()!);

  if (n.length == 6) {
    checklottery(n);
  } else {
    print("Invalid lottery numbers!!!");
  }
}
